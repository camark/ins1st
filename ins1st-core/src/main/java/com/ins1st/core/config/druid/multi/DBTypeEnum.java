package com.ins1st.core.config.druid.multi;

public enum DBTypeEnum {

    MASTER("master"), SLAVE("slave");
    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
