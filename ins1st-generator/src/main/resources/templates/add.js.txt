layui.use([ 'form'], function () {
    var o = layui.$,
        form = layui.form;

    form.verify({});

    //监听提交表单
    form.on('submit(add)',
        function (data) {
            $.ajax({
                url: Base.ctxPath + "/${map["ge"].moduleName}/${@com.ins1st.util.ColumnUtil.underline2Camel(map["ge"].tableName,true)}/save",
                type: "post",
                data: data.field,
                success: function (result) {
                    if (result.success) {
                        Base.success2(result.message);
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
            return false;
        });
})